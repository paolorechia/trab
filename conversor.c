/* converte imagem P5 8 bit para P5 16 bit.
NAO ha ganho na qualidade, serve apenas para testar
se o programa principal consegue lidar com imagens de 16 bits
------------------------------------------------------------
Exemplo de uso:
./conversor [fator] [img1] [img2]
onde img1 = origem e img2 = destino
*/
/* 
FATOR de multiplicacao da imagem.
Valores maiores podem distorcer a imagem porem garantem que os bits mais 
significativos sejam utilizados.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAXVAL 65536

void LeImagem(FILE *, int** , int, int, int, int);
void ExtraiDimensoes(FILE*, int *, int *, long int *);
void GravaImagem(FILE*, int **, int, int, long int, int);
void ImprimeMatriz(int **, int, int);
void PegaBinario(char *, int, int);

/* Le o cabecalho do arquivo e extrai o numero de linhas, de colunas e o valormaximo
*/
void ExtraiDimensoes(FILE* arquivo, int * linhas, int * colunas, long int * max)
{
    char linha[50];
    char * pter;
    int i;
    long int num;

    fgets(linha, 50, arquivo);
    while (!isdigit(linha[0]))
    {
        fgets(linha,50, arquivo);
    }
    *linhas = (int) strtol(linha, &pter, 10);
    *colunas = (int) strtol(pter, &pter, 10);

    fgets(linha, 50, arquivo);
    while (!isdigit(linha[0]))
    {
        fgets(linha,50, arquivo);
    }
    *max = (long int) strtol(linha, &pter, 10);

    return;
}

/* 
Le a imagem e armazena os valores em uma matriz
*/
void LeImagem(FILE * arquivo, int** imagem, int linhas, int colunas, int bytes, int fator)
{
    int num;
    char ch;


//    char bin[8 * bytes];
//    int size = 8 *bytes;
    int i, j;

//    if (bytes==1)
//    {
        for (i=0; i<linhas; i++)
        {
            for (j=0;j<colunas;j++)
            {
                num = fgetc(arquivo);
                imagem[i][j]= num * fator; 
            }
        }
//    }

/* codigo comentado refere-se a uma implementacao nao terminada
que conseguiria converter de 16bit para 8bit tambem
    else
    {
        for (i=0; i<linhas; i++)
        {
            for (j=0;j<colunas;j++)
            {
                for (k=0;k<size;k++)
                   bin[k]=fgetc(arquivo); 
                   num = (int) strtol(bin, NULL, 2);
                   imagem[i][j] = num; 
            }
        }
    }
*/
}

/* Grava a imagem convertida em um arquivo */
void GravaImagem(FILE* arquivo, int ** imagem, int linhas, int colunas, long int max, int bytes)
{
    int i, j;
    char bin[8 * bytes + 2];
    char * pter;
    int num;
    
//    if (bytes == 1)
//    {
        fputs("P5\n", arquivo);
        fputs("# arquivo P5 8 bits transformado para P5 16 bits (por PASR).\n", arquivo);
        fprintf(arquivo, "%d %d\n", linhas, colunas);
        fprintf(arquivo, "%ld\n", max);
        for (i=0; i<linhas; i++)
        {
            for (j=0;j<colunas;j++)
            {
                PegaBinario(bin, imagem[i][j], bytes);
                num = strtol(bin, &pter, 2); 
                fputc(num, arquivo);
                num = strtol(pter, NULL, 2); 
                fputc(num, arquivo);

            }
    //        fprintf(arquivo, "\n");
        }
//    }
/*
    else 
    {
        fputs("P5\n", arquivo);
        fputs("# arquivo 16 bits transformado para 8 bits (por PASR)\n", arquivo);
        fprintf(arquivo, "%d %d\n", linhas, colunas);
        fprintf(arquivo, "%ld\n", max);
        for (i=0; i<linhas; i++)
        {
            for (j=0;j<colunas;j++)
            {
                fprintf(arquivo, "%d ", imagem[i][j]);
            }
            fprintf(arquivo, "\n");
        }
    }
*/
}


/* Recebe um numero inteiro e armazena em uma string
o valor, representado em binario */
void PegaBinario(char * bin, int n, int bytes)
{
    int i;
    int size;
    size = bytes * 8 + 1;

    for (i = size -1; i >=(size/2 +1); i--, n>>=1)
        bin[i] = (01 & n) + '0';

    bin[size/2]=' ';

    for (i = (size/2) - 1; i >=0; i--, n>>=1)
        bin[i] = (01 & n) + '0';

    bin[size]='\0';
}


int main(int argc, char ** argv)
{
    if (argc != 4)
    {
        fprintf(stderr, "Uso: %s [fator] [img8bit] [img16bit]\n", argv[0]);
        exit(EXIT_FAILURE);
     
    }


    FILE * arquivo1;
    FILE * arquivo2;

    int ** imagem;
    int linhas;
    int colunas;
    long int max;

    int bytes;
    int fator;

    int i;

    char linha[5];




    fator = atoi(argv[1]);

    arquivo1 = fopen(argv[2], "r");

    if (arquivo1 == NULL)
    {
        fprintf(stderr, "Impossivel ler %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }

    arquivo2 = fopen(argv[3], "wb");

    if (arquivo2 == NULL)
    {
        fprintf(stderr, "Impossivel gravar %s\n", argv[3]);
        exit(EXIT_FAILURE);
    }

    // recebe primeira linha do arquivo e confere se contem a tag P5
    fgets(linha, 5, arquivo1);

    if (strcmp(linha, "P5\n") != 0)
    {
        fprintf(stderr, "%s nao eh pgm binario\n", argv[2]);
        exit(EXIT_FAILURE);
    }

    ExtraiDimensoes(arquivo1, &linhas, &colunas, &max);

    if (max < 0 || max > MAXVAL)
    {
        fprintf(stderr, "Erro: valor de MAXVAL invalido!\n");
        fprintf(stderr, "MAXVAL deve estar entre 0-65536\n");
        exit(EXIT_FAILURE);
    }

    if (max < 256)
        bytes = 2;
    else
    {
        fprintf(stderr, "Erro: arquivo ja eh 16 bits\n");
        exit(10);
    }

    if (fator == 1)
        max++;
    else
        max= max * fator;
        

    // aloca a matriz com base nas dimensoes encontradas
    imagem=malloc(linhas * sizeof(int **));
    for (i=0;i<linhas;i++)
    {
        imagem[i]=malloc(colunas *sizeof(int *));
    }

    LeImagem(arquivo1, imagem, linhas, colunas, bytes, fator);


    GravaImagem(arquivo2, imagem, linhas, colunas, max, bytes);

    fclose(arquivo1);
    fclose(arquivo2);

    for (i=0;i<linhas;i++)
    {
        free(imagem[i]);
    }
    free(imagem);

    return 0;
}
