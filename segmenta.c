/* Trabalho da disciplina CI067
 * Professor: Luiz Eduardo Oliveira
 * Aluno: Paolo Andreas Stall Rechia
 * GRR: 20135196
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAXVAL 65536

void LeImagem(FILE *, int** , int, int, int);
void ExtraiDimensoes(FILE*, int *, int *, long int *);
void GravaImagem(FILE*, int **, int, int, long int, int);
void ImprimeMatriz(int **, int, int);
void GeraVetor(int *, int, int);
void Rotula(int **, int **, int , int, int *, int);
int AchaCentroide(int, int *, int);
int Recalcula(int **, int**,  int, int, int *, int);
int Segmenta(int **, int **, int, int, int *, int);

void itobin(char *, int);
void PegaBinario(char *, int, int);


/*converte um inteiro de ateh 2 bytes para uma string com dois binarios,
cada um representando 1 byte. Sao separados por um espaco vazio (' ')
Por exemplo: 1530 eh convertido para 00000101 11111010
Esse formato permite utilizar a funcao strtol para ler os binarios
*/
void PegaBinario(char * bin, int n, int bytes) 
{
    int i;
    int size;
    size = bytes * 8 + 1;

    for (i = size -1; i >=(size/2 +1); i--, n>>=1)
        bin[i] = (01 & n) + '0';

    bin[size/2]=' ';

    for (i = (size/2) - 1; i >=0; i--, n>>=1)
        bin[i] = (01 & n) + '0';

    bin[size]='\0';
}

//converte inteiro de um byte para uma string de binario
void itobin(char * bin, int n)
{
    int i;
    int size;
    size = 8;
    for (i=size-1; i>=0; i--, n>>=1)
    {
        bin[i]=(01 & n) + '0';
    }
    bin[size]='\0';
}

// Le as primeiras linhas do arquivo, procurando pelo numero de linhas, colunas e valormaximo
// pressupoe que existe uma linha contendo o num. de linhas e de colunas
// e mais uma linha contendo o valor maximo
// NAO funciona se todos 3 valores estiverem na mesma linha
void ExtraiDimensoes(FILE* arquivo, int * linhas, int * colunas, long int * max)
{
    char linha[100];
    char * pter;
    int i;
    long int num;

    fgets(linha, 100, arquivo);
    while (linha[0] == '#')
    {
        fgets(linha,100, arquivo);
    }
    *linhas = (int) strtol(linha, &pter, 10);
    *colunas = (int) strtol(pter, &pter, 10);

    fgets(linha, 100, arquivo);
    while (linha[0] == '#')
    {
        fgets(linha,50, arquivo);
    }
    *max = (long int) strtol(linha, &pter, 10);

    return;
}


/* Le a imagem do arquivo de acordo com o numero de bits.
 * Se for 8 bits, a leitura eh realizada de modo direto,
 * caracter por caracter
 * Caso seja 16 bits, eh preciso: 1) ler um caracter, 2) converte-lo
 * para uma string de binario, 3) ler o segundo caracter, 4) converte-lo
 * para binario, 5) concatenar as duas strings, 6) transformar a string
 * do binário para um número inteiro
 */
void LeImagem(FILE * arquivo, int** imagem, int linhas, int colunas, int bytes)
{
    int num;
    int i, j;
    char bin[8 * bytes + 1];

    if (bytes == 1)
        {
            for (i=0; i<linhas; i++)
            {
                for (j=0;j<colunas;j++)
                {
                    num = fgetc(arquivo);
                    imagem[i][j]= num; 
                }
            }
        }
    else
        {
            for (i=0; i<linhas; i++)
            {
                for (j=0;j<colunas;j++)
                {
                    num = fgetc(arquivo);
                    itobin(bin, num);
//                    puts(bin);
                    num = fgetc(arquivo);
                    itobin(&bin[8], num);
                    num = strtol(bin, NULL, 2);
//                    printf("%s = %d\n", bin, num);
                    imagem[i][j]= num; 
                }
            }
        }
}

/* Assim como a leitura da imagem, a gravacao eh realizada de acordo com o numero de bits da imagem
 * Se for 8 bits, basta gravar o caracter diretamente no arquivo
 * Caso seja 16 bits, eh preciso converter a string do binario para um numero inteiro antes de
 * gravá-la
 */
void GravaImagem(FILE* arquivo, int ** imagem, int linhas, int colunas, long int max, int bytes)
{

    int i, j;
    char bin[bytes * 2 + 1];
    char * pter;
    int num;
    
    fputs("P5\n", arquivo);
    fputs("# arquivo .pgm segmentado por Paolo A. S. R.\n", arquivo);
    fprintf(arquivo, "%d %d\n", linhas, colunas);
    fprintf(arquivo, "%ld\n", max);
    if (bytes == 1)
    {
        for (i=0; i<linhas; i++)
        {
            for (j=0;j<colunas;j++)
            {
                fputc(imagem[i][j], arquivo);
            }
        }
    }
    else
        {
        for (i=0; i<linhas; i++)
        {
            for (j=0;j<colunas;j++)
            {
//                printf("%d\n", imagem[i][j]);
                PegaBinario(bin, imagem[i][j], bytes);
                num = strtol(bin, &pter, 2); 
//                printf("%s = %d + ", bin, num);
                fputc(num, arquivo);
                num = strtol(pter, NULL, 2); 
//                printf("%d\n", num);
                fputc(num, arquivo);
            }
        }
        }
}

void ImprimeMatriz(int ** imagem, int linhas, int colunas)
{
    
    int i, j;
    
    for (i=0; i<linhas; i++)
    {
        for (j=0;j<colunas;j++)
        {
            printf("%d\n", imagem[i][j]);
        }
    }
}

void GeraVetor(int * centroides, int num_centroides, int max)
{
    int i;

    srand(time(NULL)); //inicializa seed para numeros randomicos
    for (i=0; i < num_centroides; i++)
    {
        centroides[i]=rand() % max;
    }

}

/* O Algoritmo KMeans foi implementado em 4 subfuncoes: Rotula, AchaCentroide, Recalcula e Segmenta 
 * Cada funcao esta contida dentro de outra(s), com excecao da Segmenta que eh a principal.
 *
 * A funcao AchaCentroide recebe um valor inteiro, busca sequencialmente no vetor de centroides
 * e retorna o centroide de valor mais proximo
 */
int AchaCentroide(int buscado, int * centroides, int num_centroides)
{
    int i;
    long int menor;
    int dif;
    int pos;

    menor = 70000;

    pos = -1;

    for (i = 0; i< num_centroides; i++)
    {
        dif = abs(buscado - centroides[i]);
        if (dif < menor)
        {
            menor = dif;
            pos = i;
        }
    }

    return pos;
}

/*
* A funcao rotula gera a matriz rotulo a partir dos parametros recebidos
*/
void Rotula(int ** imagem, int ** rotulo, int linhas, int colunas, int * centroides, int num_centroides)
{
    int i, j;
    int pos;

    for (i=0;i<linhas;i++)
    {
        for (j=0;j<colunas;j++)
        {
            if ((pos = AchaCentroide(imagem[i][j], centroides, num_centroides)) == -1)
            {
                fprintf(stderr, "Erro: centroide nao encontrado\n");
                exit(1);
            }
            else
            {
                rotulo[i][j]=pos;
            }
        }
    }
}


/* A funcao Recalcula eh responsavel por recalcular o valor dos centroides a partir da matriz rotulo
 * e retorna 1 caso a operacao suceda.
 * Caso nao haja diferenca de valores retorna 0, indicando o termino da segmentacao
 */ 
int Recalcula(int ** imagem, int ** rotulo, int linhas, int colunas, int * centroides, int num_centroides)
{
    int media;
    int soma;
    int total;
    int recalculou = 0;
    int i, j, k;

    for (k = 0; k < num_centroides; k++)
    {
        soma = 0;
        total = 0;
        for (i=0; i < linhas; i++)
            {
                for (j=0; j< colunas; j++)
                {
                    if (k == rotulo[i][j])
                    {
                        soma += imagem[i][j];
                        total++; 
                    }
                }
            }
        if (!total)
            continue;
        media = soma/total;
        if (media != centroides[k])
        {
            recalculou = 1;
            centroides[k]=media;
        }
    }
            
    return recalculou;
}

/* A funcao Segmenta tem duas atribuicoes:
 * 1 - Rodar um loop que rotula a imagem original tantas vezes quanto possivel
 * 2 - Apos, substitui a imagem original pela imagem segmentada
 */
int Segmenta(int ** imagem, int ** rotulo, int linhas, int colunas, int * centroides, int num_centroides)
{
    int i, j;
    int numero_rotulos = 1;

    Rotula(imagem, rotulo, linhas, colunas, centroides, num_centroides);
    while ((Recalcula(imagem, rotulo, linhas, colunas, centroides, num_centroides) !=0))
    {
    Rotula(imagem, rotulo, linhas, colunas, centroides, num_centroides);
    numero_rotulos++; 
    }
    
    for (i=0; i< linhas; i++)
    {
        for (j=0; j< colunas; j++)
        {
            imagem[i][j]=centroides[rotulo[i][j]];
        }
    }
    return numero_rotulos;
}

int main(int argc, char ** argv)
{
    if (argc != 4)
    {
        fprintf(stderr, "Uso: %s [imgO] [num_centroides] [imgR]\n", argv[0]);
        exit(EXIT_FAILURE);
    }


    FILE * arquivo1;
    FILE * arquivo2;
    FILE * log;

	/* o log foi utilizado para facilitar a implementacao, nao
	 * eh necessario. Permanece no programa apenas por conveniencia
	 */

    int ** imagem;
    int linhas;
    int colunas;
    long int max;
    int bytes;

    int ** rotulo;
    
    int i;

    int num_centroides;
    int * centroides;

    char linha[5];

	/* leitura dos parametros */

    num_centroides = atoi(argv[2]);

    if (num_centroides < 2 || num_centroides > 10)
    {
        fprintf(stderr, "Numero de num_centroides invalido\n");
        fprintf(stderr, "Deve ser entre 2-10\n");
        exit(EXIT_FAILURE);
    }

	/* manuseio inicial dos arquivos */

    log = fopen("log_segmenta.txt", "w");

    if (log == NULL)
    {
        fprintf(stderr, "Impossivel gravar log");
        exit(EXIT_FAILURE);
    }

    fprintf(log, "log inicializado c/ sucesso\n");

    arquivo1 = fopen(argv[1], "rb");

    if (arquivo1 == NULL)
    {
        fprintf(stderr, "Impossivel ler %s\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    fprintf(log, "%s aberto c/ sucesso\n", argv[1]);

    arquivo2 = fopen(argv[3], "wb");

    if (arquivo2 == NULL)
    {
        fprintf(stderr, "Impossivel gravar %s\n", argv[3]);
        exit(EXIT_FAILURE);
    }

    fprintf(log, "%s arquivo aberto c/ sucesso\n", argv[3]);


	/* inicio da leitura da imagem */

    fgets(linha, 5, arquivo1);
    
    if (strcmp(linha, "P5\n") != 0) // confere se a imagem eh .pgm raw. Assume a
	   								// existencia de uma linha para o P5.
    {
        fprintf(stderr, "%s nao eh .pgm\n", argv[2]);
        fprintf(log, "Primeira linha: '%s'\n", linha);
        exit(EXIT_FAILURE);
    }
    
    fprintf(log, "eh pgm... iniciando leitura\n");

    ExtraiDimensoes(arquivo1, &linhas, &colunas, &max);

    fprintf(log, "dimensoes da imagem\n");
    fprintf(log, "linhas = %d; colunas = %d\n; max = %ld\n", linhas, colunas, max);

    if (max < 0 || max > MAXVAL)
    {
        fprintf(stderr, "Erro: valor de MAXVAL invalido!\n");
        fprintf(stderr, "MAXVAL deve estar entre 0-65536\n");
        exit(EXIT_FAILURE);
    }
    if (max < 256)
        bytes = 1;
    else
        bytes = 2;
        

    fprintf(log, "Alocando memoria ...\n");

    imagem=malloc(linhas * sizeof(int **));
    for (i=0;i<linhas;i++)
    {
        imagem[i]=malloc(colunas *sizeof(int *));
    }
    rotulo=malloc(linhas*sizeof(int **));
    for (i=0;i<linhas;i++)
    {
        rotulo[i]=malloc(colunas*sizeof(int *));
    }
    centroides=malloc(num_centroides*sizeof(int *));

    fprintf(log, "Lendo imagem...\n");

    LeImagem(arquivo1, imagem, linhas, colunas, bytes);

	/* termino da leitura */

    fprintf(log, "Gerando vetor... \n");
    GeraVetor(centroides, num_centroides, max);

	/* inicio do algoritmo Kmeans */

    fprintf(log, "Vetor gerado: \n");
    for (i=0; i<num_centroides; i++)
    {
        fprintf(log, "%d ", centroides[i]);
    }
    fprintf(log, "\n");

    fprintf(log, "Rotulando imagem...\n");

    fprintf(log, "%d rotulos realizados\n", Segmenta(imagem, rotulo, linhas, colunas, centroides, num_centroides));

	/* fim do algoritmo Kmeans */

	
    fprintf(log, "Gravando Imagem... \n");
    GravaImagem(arquivo2, imagem, linhas, colunas, max, bytes);

	/* fecha arquivos, desaloca memoria */

    fclose(arquivo1);
    fclose(arquivo2);
    
    for (i=0;i<linhas;i++)
    {
        free(imagem[i]);
        free(rotulo[i]);
    }
    free(imagem);
    free(rotulo);

    return 0;
}
